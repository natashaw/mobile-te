package com.example.physiqtecodingchallenge

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.not
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class InstrumentedTest {

    @Rule
    @JvmField
    val rule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    /* SANITY CHECK */
    // PASSED
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.physiqtecodingchallenge", appContext.packageName)
    }

    // PASSED
    @Test
    fun canEnterValueOnField() {
        // lumped with performing boundary test by entering long values
        onView(withId(R.id.edittext_numerator)).perform(typeText("999999999999"))
        onView(withId(R.id.edittext_numerator)).check(matches(withText("999999999999")))
        onView(withId(R.id.edittext_divisor)).perform(typeText("999999999999"))
        onView(withId(R.id.edittext_divisor)).check(matches(withText("999999999999")))
    }

    // PASSED
    @Test
    fun canPerformDivisionOnButtonClick() {
        onView(withId(R.id.edittext_numerator)).perform(typeText("9.0"))
        onView(withId(R.id.edittext_divisor)).perform(typeText("3.0"))
        onView(withId(R.id.button_divide))
            .perform(click())
            .check(matches(isClickable()))
        onView(withId(R.id.textview_result)).check(matches(withText("3.0")))
    }

    //
    @Test
    fun canClearInputFields() {
        onView(withId(R.id.edittext_numerator)).perform(typeText("123"))
        onView(withId(R.id.edittext_divisor)).perform(typeText("123"))
        onView(withId(R.id.button_clear)).perform(click())
        onView(withId(R.id.edittext_numerator)).check(matches(withText("")))
        onView(withId(R.id.edittext_divisor)).check(matches(withText("")))
    }

    /* BOUNDARY TEST */
    // FAILED: result is displaying 999..+E instead of the actual long number
    @Test
    fun canDisplayLongResult() {
        onView(withId(R.id.edittext_numerator)).perform(typeText("999999999999"))
        onView(withId(R.id.edittext_divisor)).perform(typeText("1"))
        onView(withId(R.id.button_divide)).perform(click())
        onView(withId(R.id.textview_result)).check(matches(withText("999999999999.0")))
    }

    // FAILED: user can still click the divide button despite of empty input fields
    @Test
    fun noClickWithEmptyFields() {
        onView(withId(R.id.button_divide)).check(matches(not(isClickable())))
    }

    /* INTERFACE TEST */
    // FAILED: MainActivity needs to specify the number of decimals the result would be rounded into
    @Test
    fun canDisplayResultWithDecimals() {
        onView(withId(R.id.edittext_numerator)).perform(typeText("856"))
        onView(withId(R.id.edittext_divisor)).perform(typeText("116"))
        onView(withId(R.id.button_divide))
            .perform(click())
            .check(matches(isClickable()))
        onView(withId(R.id.textview_result)).check(matches(withText("7.38")))
    }


}
