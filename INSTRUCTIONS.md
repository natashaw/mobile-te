Coding Challenge
===

Overview
---

The "physIQ Coding Challenge" is a simple app. It contains two input fields for a numerator and a 
divisor, a button to perform the division, a textview element that displays the result, and a button 
that clears all the fields.

### Coding exercise

**1. Document a list of test cases for the coding challenge app.**

_Under InstrumentedTest.kt the written automated tests are as follows:_
* useAppContext -- verifies that the application context returns the correct package name
* canEnterValueOnField -- verifies that: 1. long input values can be displayed on the text field accurately; 2. input of values can be done without error;
* canPerformDivisionOnButtonClick -- verifies that the "divide" button results in the displaying the result, and displaying it correctly
* canClearInputFields -- verifies that the input fields are cleared once "clear" button is clicked
* canDisplayLongResult -- verifies whether a long result can be displayed correctly on the app
* noClickWithEmptyFields -- verifies clickability of the "divide" button when there is no input. App should've handled non-designed user inputs / actions properly without crashing
* canDisplayResultWithDecimals -- checks how the app displays results with long decimal points
* divideByZero -- validate divisor input, see if the app handles division of '0' properly

_Sorry that it took me some time to finish this coding challenge. This week I had some problems with accommodation and some logistics on top of an intensive language course I'm taking in Berlin. Given more time, I'd also add the following tests:_

* performance test
* how the app handles user input of non-numerical values

_The detailed documentation of the results of the written test methods above can be found in the XML file included on the root directory of this repo._

**2. Write at least one automated test.**
   1. Download and install Android Studio (https://developer.android.com/studio)
   2. Set up an Android Virtual Device (https://developer.android.com/studio/run/managing-avds)
   3. Implement the tests with Espresso (https://developer.android.com/training/testing/espresso)

**3. Run the tests with Gradle cli or in Studio**

Please submit the test files you wrote or recorded and test results (logged output or xml reports). If you find any bugs please please document them, too.

### Written exercises

**1. Describe how you would run these tests in a continuous integration pipeline.**

_One would need to utilize a tool that hosts both the codebase and the automated testing scripts, i.e., a CI service, which automatically runs tests as configured on every push to the main repository. On Gitlab, for example, the configuration of build/test schedule is defined in '.gitlab-ci.yml' file. First one should define the stages, and then define the specifics of each stage, which also includes 'stage: test', and any custom gradlew script as required._

**2. If the division function were moved to a REST api that you were also responsible for testing, what tools would you use to test it.**

_REST API is honestly a new field for me; however, from doing some research, it seems like the combination of the following is popularly used in testing REST API request:_
* Retrofit -- HTTP REST client, used to perform API requests
* Gson -- JSON deserializer
* Mockito -- contains feature that enables mocking of requests